/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef LARREADOUTGEOMETRY_FCALTUBECONSTLINK_H
#define LARREADOUTGEOMETRY_FCALTUBECONSTLINK_H
#include "LArReadoutGeometry/FCALTube.h"
#include "GeoModelKernel/GeoIntrusivePtr.h"
using FCALTubeConstLink=GeoIntrusivePtr<const FCALTube>;
#endif
