#include "DerivationFrameworkTau/TauTruthMatchingWrapper.h"
#include "DerivationFrameworkTau/TauIDDecoratorWrapper.h"
#include "DerivationFrameworkTau/TauThinningTool.h"

using namespace DerivationFramework;

DECLARE_COMPONENT( TauTruthMatchingWrapper )
DECLARE_COMPONENT( TauIDDecoratorWrapper )
DECLARE_COMPONENT( TauThinningTool )
